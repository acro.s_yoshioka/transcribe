import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  results: Array<ResultSummary>;
  host: String;

  constructor(private http: HttpClient, private router:Router) {
    this.host = "https://7f3zix5h9g.execute-api.us-west-2.amazonaws.com/dev/"
    this.results = new Array<ResultSummary>();
  }

  ngOnInit() {
    this.getSummaryResults();
  }

  getSummaryResults() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    let url = this.host + "summary"
    this.http.get<Response>(url, httpOptions).subscribe(
      data => {
        let response = data;
        response.results.forEach(result => {
          this.results.push(result);
        })
      }
    )
  }

  navigate(uuid_str){
    this.router.navigate(['detail/' + uuid_str]);
     this.router.navigateByUrl('detail/' + uuid_str);
    }

}

class Response {
  results: Array<ResultSummary>
}

class ResultSummary {
  uuid_str: String;
  title: String;
  positive: Number;
  negative: Number;
  neutral: Number;
  mixed: Number;
}