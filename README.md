# 概要

音声ファイルから、文字起こし→感情分析を実施して、 結果を表示する

# デプロイ手順

## 環境準備
### アカウント用意
AWSのアカウント情報を取得し、AWS CLIでcredentialsの設定を行っている。  
`aws configure`

### 必要なライブラリのインストール
serverless frameworkのインストール  
`npm install -g serverless`

x-ray有効化のためのプラグインインストール  
`npm install aws-sdk aws-xray-sdk serverless-plugin-tracing`

python aws x-ray sdkのダウンロード  
`pip install aws-xray-sdk -t .`

## デプロイ
Serverless Frameworkを使用してデプロイ
`sls deploy`

## S3のCORS設定
署名付きURLでブラウザからアップロードできるようにするためにS3のCORSの設定を行う。
AWSのS3の画面から「uploaded-recorded」バケットを選択し、「アクセス権限」→「CORSの設定」で下記を入力する。  
```
<?xml version="1.0" encoding="UTF-8"?>
<CORSConfiguration xmlns="http://s3.amazonaws.com/doc/2006-03-01/">
<CORSRule>
    <AllowedOrigin>*</AllowedOrigin>
    <AllowedMethod>HEAD</AllowedMethod>
    <AllowedMethod>GET</AllowedMethod>
    <AllowedMethod>PUT</AllowedMethod>
    <AllowedMethod>POST</AllowedMethod>
    <AllowedHeader>*</AllowedHeader>
</CORSRule>
</CORSConfiguration>
```
  
