import json
import boto3
import datetime
import uuid

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()

comprehend_client = boto3.client("comprehend")
s3_client = boto3.client("s3")
dynamo_client = boto3.client('dynamodb')

def run(event, context):
    record_file = event["Records"][0]["s3"]["object"]["key"]
    bucket_name = event["Records"][0]["s3"]["bucket"]["name"]
    url = f"s3://{bucket_name}/{record_file}"
    print(url)

    transcribed_result = s3_client.get_object(Bucket=bucket_name, Key=record_file)
    text = transcribed_result["Body"].read().decode()
    transcribe = json.loads(text)

    segments = [{"start": x["start_time"], "end": x["end_time"]} for x in transcribe["results"]["speaker_labels"]["segments"]]
    sentences = []
    sentence_index = 0
    buffer = []
    for item in transcribe["results"]["items"]:
        if "end_time" in item:
            buffer.append(item["alternatives"][0]["content"])
            if item["end_time"] == segments[sentence_index]["end"]:
                sentences.append("".join(buffer))
                buffer = []
                sentence_index += 1
    print(sentences)

    title = f"{transcribe['jobName']} {datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')}"
    uuid_str = str(uuid.uuid4())

    sent_buffer = []
    start_index = 0
    for index, sentence in enumerate(sentences):
        sent_buffer.append(sentence)
        if len(sent_buffer) == 25:
            analyze_sentences(sent_buffer, title, start_index, uuid_str)
            sent_buffer = []
        start_index += 1
    
    analyze_sentences(sent_buffer, title, start_index, uuid_str)

    s3_client.delete_object(Bucket=bucket_name, Key=record_file)

def analyze_sentences(sentences, title, start_index, uuid_str):
    response = comprehend_client.batch_detect_sentiment(
        TextList=sentences,
        LanguageCode="ja"
    )

    for index, result in enumerate(response["ResultList"]):
        sentence_index = str(start_index + index - len(sentences) + 2)
        body = {
            "id": {"S": f"{uuid_str}-{sentence_index}"},
            "sentence": {"S": sentences[index]},
            "positive": {"N": str(result["SentimentScore"]["Positive"])},
            "negative": {"N": str(result["SentimentScore"]["Negative"])},
            "neutral": {"N": str(result["SentimentScore"]["Neutral"])},
            "mixed": {"N": str(result["SentimentScore"]["Mixed"])},
            "title": {"S": title},
            "uuid_str": {"S": uuid_str},
            "index": {"N": sentence_index}
        }
        dynamo_client.put_item(TableName="transcription_table", Item=body)