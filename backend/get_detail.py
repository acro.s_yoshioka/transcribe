import json
import boto3
from boto3.dynamodb.conditions import Key, Attr

dynamo_client = boto3.client('dynamodb')

def run(event, context):
    response = dynamo_client.scan(
        TableName="transcription_table", 
        FilterExpression="uuid_str = :uuid_str",
        ExpressionAttributeValues={":uuid_str": {"S": event["pathParameters"]["uuid_str"]}}
    )

    items = response["Items"]

    results = []

    for item in items:
        results.append(
            {
                "positive": round(float(item["positive"]["N"]), 2),
                "negative": round(float(item["negative"]["N"]), 2),
                "neutral": round(float(item["neutral"]["N"]), 2),
                "mixed": round(float(item["mixed"]["N"]), 2),
                "sentence": item["sentence"]["S"],
                "index": int(item["index"]["N"])
            }
        )

    results.sort(key=lambda x: x["index"])

    response = {"results": results, "title": items[0]["title"]["S"]}
    return {
        'isBase64Encoded': False,
        "statusCode": "200",
        "headers": {"Access-Control-Allow-Origin": "*"},
        "body": json.dumps(response)
    }