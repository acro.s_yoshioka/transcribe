import boto3
from boto3.session import Session
from botocore.client import Config
import uuid
import os

REGION_NAME = "us-west-2"
S3_BUCKET_NAME = "uploaded-recorded"
IAM_ROLE_ARN = "arn:aws:iam::198390641290:role/transcribe-dev-us-west-2-lambdaRole"
DURATION_SECONDS = 900

def run(event, context):
    file_name = event["pathParameters"]["file_name"]

    client = boto3.client("sts")

    response = client.assume_role(RoleArn=IAM_ROLE_ARN,
                                  RoleSessionName=file_name,
                                  DurationSeconds=DURATION_SECONDS)
 
 
    session = Session(aws_access_key_id=response['Credentials']['AccessKeyId'],
                      aws_secret_access_key=response['Credentials']['SecretAccessKey'],
                      aws_session_token=response['Credentials']['SessionToken'],
                      region_name=REGION_NAME)

    s3 = session.client('s3', config=Config(signature_version='s3v4'))

    url = s3.generate_presigned_url(ClientMethod = 'put_object', 
                                    Params = {'Bucket' : S3_BUCKET_NAME, 'Key' : file_name}, 
                                    ExpiresIn = DURATION_SECONDS, 
                                    HttpMethod = 'PUT')

    print(url)
    return {
       'statusCode': 200,
       'isBase64Encoded': False,
       'headers': {
           'Content-Type': 'text/html; charset=utf-8',
           "Access-Control-Allow-Origin": "*"
        },
        'body': '{}\n'.format(url)
    }