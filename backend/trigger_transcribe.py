import json
import boto3

from aws_xray_sdk.core import xray_recorder
from aws_xray_sdk.core import patch_all

patch_all()

transcribe_client = boto3.client('transcribe')
def run(event, context):
    record_file = event["Records"][0]["s3"]["object"]["key"]
    bucket_name = event["Records"][0]["s3"]["bucket"]["name"]
    url = f"https://s3.us-west-2.amaoznaws.com/{bucket_name}/{record_file}"
    print(url)

    transcribe_client.start_transcription_job(
        TranscriptionJobName = record_file.split(".")[0],
        Media={"MediaFileUri": url},
        MediaFormat="wav",
        LanguageCode="ja-JP",
        OutputBucketName="transcribed-record",
        Settings={
            "MaxSpeakerLabels": 2,
            "ShowSpeakerLabels": True
        }
    )

    while True:
        status = transcribe_client.get_transcription_job(TranscriptionJobName=record_file.split(".")[0])
        if status['TranscriptionJob']['TranscriptionJobStatus'] in ['COMPLETED', 'FAILED']:
            break
        print("Not ready yet...")
        time.sleep(5)
    print(status)
